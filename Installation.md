# Installation of CPAchecker {#instcpa}

You can always find the most up-to-date installation instructions for CPAchecker at:
<https://gitlab.com/sosy-lab/software/cpachecker/blob/trunk/INSTALL.md>

Nonetheless we will describe in the following how you can get it running (and check that it is working) on Windows and Linux.
In case you have any problems (e.g. in case the instructions here are outdated) just write an email to <cpachecker-users@googlegroups.com>.
This is always a good idea if you have any CPAchecker-related questions.

## Windows {#instcpawin}

*Disclaimer:* There might be differences in the behavior of the Windows version and the Linux version. In this document we will assume that you use the Linux version of CPAchecker, so if you can manage to run Linux, e.g. via a virtual machine, please proceed at [Installation of CPAchecker for Linux](#instcpalinux).

- Get the Java SDK, e.g. from [adoptopenjdk.net](https://adoptopenjdk.net/releases.html?variant=openjdk11&jvmVariant=hotspot). Choose Windows as operating system and x86 or x64 as architecture depending on your hardware. If your hardware is not very old you probably need x64. You can find out the architecture of your windows installation by going to the System overview (press Windows Key + Pause), where it will tell you whether your operating system is 64-bit (=x64) or 32-bit (x86). Download the JRE in the `.msi` format and install it:
![Image of the download page with the right download button highlighted](./images/download_jre_here.png)
- Download the latest windows version of CPAchecker from the [CPAchecker website](https://cpachecker.sosy-lab.org/download.php), which by the time of writing this is [CPAchecker-2.1.1-windows.zip](https://cpachecker.sosy-lab.org/CPAchecker-2.1.1-windows.zip).
- Extract the downloaded file.

The contents of the extracted folder for CPAchecker should look like this:

![Contents of the CPAchecker folder](./images/cpafolder.png)
<!--<img src="./cpafolder.png" style="width:100px;"/>-->

### Is it working?

In order to check if everything works as expected, open the powershell (windows key + r, type in powershell, press enter), go into the CPAchecker directory and execute the following command:

```bash
    .\scripts\cpa.bat -predicateAnalysis .\doc\examples\example.c -setprop solver.solver=SMTInterpol -setprop cpa.predicate.encodeBitvectorAs=INTEGER -setprop cpa.predicate.encodeFloatAs=RATIONAL
```

The additional options with `-setprop` are necessary under Windows only, the default solver for CPAchecker is not yet supported.
The output should look like in the following picture and a new folder named output should appear in the CPAchecker directory:

![powershell commands and new folder in CPAchecker directory](./images/powershell.png)
<!--this commend magically make the image span the whole width (don't ask)-->

The output folder contains a file named Report.html which can be viewed in the browser.

## Linux {#instcpalinux}

- install a Java Runtime Environment (Ubuntu: `sudo apt-get install openjdk-11-jre`)

```bash
        sudo apt-get install openjdk-11-jre
```

  (if you are planning to develop CPAchecker, you would need the `openjdk-11-jdk` instead, but that is not covered in this tutorial)

- Download the latest release of CPAchecker from the [website](https://cpachecker.sosy-lab.org/download.php). As the time of writing the latest version for Linux is [CPAchecker-2.1.1-unix.zip](https://cpachecker.sosy-lab.org/CPAchecker-2.1.1-unix.zip) (ca. 103MB). After downloading, unpack it whereever you prefer:

```bash
~ $ curl -O https://cpachecker.sosy-lab.org/CPAchecker-1.9.1-unix.zip
~ $ unzip CPAchecker-1.9.1-unix.zip
```

### Is it working?

In order to check if everything works as expected, go into the CPAchecker directory and try if this simple analysis will run successfully:

```bash
    ~ $ cd CPAchecker-1.9.1-unix
    ~/CPAchecker-1.9.1-unix $ ./scripts/cpa.sh -predicateAnalysis doc/examples/example.c
```

The output should look like this:

```
Running CPAchecker with default heap size (1200M). Specify a larger value with -heap if you have more RAM.
Running CPAchecker with default stack size (1024k). Specify a larger value with -stack if needed.
Language C detected and set for analysis (CPAMain.detectFrontendLanguageIfNecessary, INFO)

Using the following resource limits: CPU-time limit of 900s (ResourceLimitChecker.fromConfiguration, INFO)

CPAchecker 1.9.1 / predicateAnalysis (OpenJDK 64-Bit Server VM 11.0.8) started (CPAchecker.run, INFO)

Parsing CFA from file(s) "doc/examples/example.c" (CPAchecker.parse, INFO)

Using predicate analysis with MathSAT5 version 5.5.4 (bd863fede57e) (Feb 21 2019 15:05:40, gmp 6.1.0, gcc 4.8.5, 64-bit, reentrant) and JFactory 1.21. (PredicateCPA:PredicateCPA.<init>, INFO)

Using refinement for predicate analysis with PredicateAbstractionRefinementStrategy strategy. (PredicateCPA:PredicateCPARefiner.<init>, INFO)

Starting analysis ... (CPAchecker.runAlgorithm, INFO)

Stopping analysis ... (CPAchecker.runAlgorithm, INFO)

Verification result: TRUE. No property violation found by chosen configuration.
More details about the verification run can be found in the directory "./output".
Graphical representation included in the file "./output/Report.html".
```

A new folder named `output` should appear in the CPAchecker directory. This folder contains a file named Report.html that can be viewed in the browser
and visualizes several aspects of the performed verification.

