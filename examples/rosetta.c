#include "stdlib.h"

// nondet functions, one for each data type,
// cf. __VERIFIER_nondet_X() at https://sv-comp.sosy-lab.org/2022/rules.php
extern int __VERIFIER_nondet_int();
extern char __VERIFIER_nondet_char();
extern long __VERIFIER_nondet_long();
//...


// this is not a special function. In SV-COMP, the reachability property
// specifies that this function shall never be called, i.e., in LTL:
// G ! call(reach_error())
void reach_error(){};


// this is also not a special function (despite the __VERIFIER prefix!),
// since we can encode assertions by using reach_error
void __VERIFIER_assert(int cond) {
  if (!cond) {
    reach_error();
  }
}

// we do not need special functions for implementing assumptions.
// one way of implementing assumptions:
void assume_loop_if_not(int cond) {
  if (!cond) {
    while (1) {};
  }
}
// if termination should not be affected:
void assume_abort_if_not(int cond) {
  if (!cond) {
    abort();
  }
}

int main() {
  
  // # (Signed Integer) Overflows
 
  int x = 2147483647; //INT_MAX on usual 32bit and 64bit systems
  x++; // this is a violation of no-overflow (G ! overflow)

  // Reachability

  unsigned int y = __VERIFIER_nondet_int();
  __VERIFIER_assert(y*y<y);
  // not an overflow with undefined behavior since y is unsigned;
  //this is a violation of G ! call(reach_error())
  
  // # Memory Cleanup

  int * p = malloc(sizeof(int));
  // we never free this memory, so this is a violation of G valid-memcleanup
  // in case we reach the return statement of main
                                 

  // # Memory Safety
  int *q = malloc(sizeof(int));
  q = 0; // we lose track of the memory, no way to free it anymore
         // => violation of G valid-memtrack

  int *r = malloc(sizeof(int));
  free(r);
  y= *r; // we dereference an invalid pointer => violation of G valid-deref

  free(r);
  // this is an example of an invalid free => violation of G valid-free
  

  // # Termination

  while (1) {}; // this is an infinite loop => violation of: F end

  return 0;
}
