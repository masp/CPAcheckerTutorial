# Installation of KLEE {#instklee}

## Docker

KLEE is best used with a docker container (so they say).
How to install docker in Ubuntu is described [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04).
Installing docker on Ubuntu is not that easy unfortunately, because it is not in the regular repository *sigh*
Installation of KLEE using docker is described [here](http://klee.github.io/docker/).
It boils down to:

    docker pull klee/klee
    docker run --rm -ti --ulimit='stack=-1:-1' klee/klee

The container can be left with ctrl+d or exit, as you would exit any terminal.
If you want to make a folder on the host, e.g. the current working directory,  accessible to the klee container:

    docker run --rm -ti --ulimit='stack=-1:-1' -v $(pwd):/home/klee/dir klee/klee

This will make the current working directory on the host available under /home/klee/dir in the container

## With binaries contained in TBF

KLEE binaries are contained in tbf:

    git clone https://github.com/sosy-lab/tbf.git tbf
    ls tbf/klee/bin
    clang  gen-random-bout  kleaver  klee  klee-replay  klee-stats  ktest-tool

Unfortunately, these depend on shared libraries, so we need to adapt both the `PATH` and `LD_LIBRARY_PATH` of the current bash session to make use of the KLEE binaries:

    export PATH=$(pwd)/tbf/klee/bin:$PATH
    export LD_LIBRARY_PATH=$(pwd)/tbf/klee/lib:$LD_LIBRARY_PATH
    export KLEE_RUNTIME_LIBRARY_PATH=$(pwd)/tbf/klee/lib
    klee --help #for testing if this works, should generate help output

Alternatively there is a script named `run_klee` in TBF that lets us run clang followed by klee when we supply it the path to a c program.
It is a simple bash script that sets the environment variables like we just did manually.


# Installation of TBF {#insttbf}

TBF can be cloned as git repository:

    git clone https://github.com/sosy-lab/tbf.git tbf

See the README.md for a description of basic functionality.
E.g. for checking a file using KLEE:

    ./run_iuv -i klee --execution programtocheck.c

# Installation of AFL {#instafl}

Get the latest tarball:

    curl -O http://lcamtuf.coredump.cx/afl/releases/afl-latest.tgz
 
 Notice that https is not available, thus the download is pure, untrusted http!?!
 Maybe use this unofficial git repo instead:
 
     git clone https://github.com/mcarpenter/afl
     cd afl
     make -j5
