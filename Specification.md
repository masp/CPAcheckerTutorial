# Specification and Instrumentation

## Specification Automata {#specaut}

CPAchecker features an elaborate automaton language with which
specifications can be crafted. This supports querying certain
analyses/domains for information and then updating the
specification automaton state according to the outcome.

The documentation of these specification automata can be found here:
[SpecificationAutomata.md](https://gitlab.com/sosy-lab/software/cpachecker/-/blob/trunk/doc/SpecificationAutomata.md)

## SV-COMP Properties {#prp}

For the competition on software verification, certain specifications
are provided as so-called property files, which contain some
simple form of LTL specification. In practice, these do not change very
often, so they are currently internally mapped to existing specification
automata inside CPAchecker.

The SV-COMP properties are described at:
[SV-COMP Rules Page](https://sv-comp.sosy-lab.org/2022/rules.php)
(this link might be outdated, adapt the year if required to ensure you view the most-recent version)

The most-commonly used properties for verification are as follows:

- [unreach-call.prp](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/properties/unreach-call.prp)
- [no-overflow.prp](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/properties/no-overflow.prp)
- [termination.prp](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/properties/termination.prp)
- [valid-memcleanup.prp](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/properties/valid-memcleanup)
- [valid-memsafety.prp](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/properties/valid-memsafety)

## Instrumentation {#inst}

For telling the verifier what one wants to check regarding the input program,
often minor instrumentations are necessary.
For example

## Example {#ex}

For getting a better feeling of how instrumentation works together with the most-commonly used properties,
we can have a look at the following example program [rosetta.c]({{book.url}}/examples/rosetta.c):

[include](./examples/rosetta.c)



