#Introduction

This document is a work in progress.
The goal is to give some insights into:

- How to verify software
- How to use CPAchecker
- How to use similar tools, e.g. in the context of testing
- Show the differences and similarities between them


