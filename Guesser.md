# Program Instrumentation Challenge {#guesser}

In this project, we will look at a small example program written in C.
We will apply the concepts explained in the chapter on Specification/Instrumentation
and prepare the program such that it can be verified
with an automatic verifier like CPAchecker.
For verification, we will use the analyses explained in the chapter on Model Checking.

## The Game {#game}

The program ([guesser.c](examples/guesser.c)) is a small number guessing game.
The computer randomly selects a number smaller than
1000 and the user can make iterative guesses
until they pick the correct number.
The program also does not allow the user to enter a number that is further from
the correct number than any of the previous guesses.

Here is an example listing of how to compile the program and run it:

```bash
gcc guesser.c -o guesser
./guesser
Welcome, please guess my number from 0 to 999
Enter an integer:
500
Too low!Enter an integer:
750
Too high!Enter an integer:
625
Too high!Enter an integer:
570
Too high!Enter an integer:
535
Too low!Enter an integer:
555
Too high!Enter an integer:
550
You guessed correctly!
```

## Program with Challenge {#prog}

In the following, you see a listing of the program ([guesser.c](examples/guesser.c)).
It already contains comments with instructions on what needs to be done.

[include](./examples/guesser.c)


